<html lang="fr">
<head>
	<meta charset="utf-8">

	<title>Intégration</title>
	<meta name="author" content="Théo Pazzaglia">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<link rel="stylesheet" href="styles/style.css">


</head>

<body>
<div id="loader">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 173.8 112.6" style="enable-background:new 0 0 173.8 112.6;" xml:space="preserve">
<style type="text/css">
    .st0{display:none;}
    .st1{display:inline;fill:#E7F2F1;}
    .st2{fill:#605F5F;}
    .st3{fill:#C1B9B9;}
    .st4{fill:#B5ABAA;}
    .st5{fill:#323232;}
    .st6{fill:#E5E5E5;}
    .st7{fill:#35C7E1;}
    .st8{fill:#28B1BF;}
    .st9{fill:#424242;}
    .st10{opacity:0.5;}
    .st11{fill:#898686;}
    .st12{fill:#FFFFFF;}
    .st13{opacity:0.3;}
</style>
        <g id="Background" class="st0">
            <rect x="-163.1" y="-193.7" class="st1" width="500" height="500"/>
        </g>
        <g id="Objects">
            <g>
                <g>
                    <g>
                        <g class="roue roue-arriere">
                            <g>
                                <polygon class="st2" points="34.5,78 34.9,78 33.9,48.2 33.5,48.2 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="34.9,78.1 41.6,49 41.3,48.9 34.5,78 						"/>
                            </g>
                            <g>

                                <rect x="26.8" y="64.7" transform="matrix(0.4716 -0.8818 0.8818 0.4716 -35.1719 71.0534)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="30" y="66.9" transform="matrix(0.6838 -0.7297 0.7297 0.6838 -34.8059 53.9714)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="32.4" y="70" transform="matrix(0.8493 -0.5279 0.5279 0.8493 -29.8995 35.5586)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>
                                <rect x="34" y="73.5" transform="matrix(0.957 -0.29 0.29 0.957 -19.2722 17.3635)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="34.7" y="77.3" transform="matrix(0.9995 -3.242296e-02 3.242296e-02 0.9995 -2.488 1.6481)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="49" y="66.5" transform="matrix(0.2264 -0.974 0.974 0.2264 -41.2434 110.8936)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>

                                <rect x="47.6" y="70.1" transform="matrix(0.4725 -0.8813 0.8813 0.4725 -49.7355 87.0121)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>

                                <rect x="45.4" y="73.3" transform="matrix(0.6823 -0.731 0.731 0.6823 -50.0199 61.3237)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="50.2,103.5 50.6,103.2 34.8,77.9 34.5,78.1 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="43.1,106.6 43.5,106.5 34.9,78 34.5,78.1 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="35.4,107.8 35.8,107.8 34.9,78 34.5,78 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="28.1,107.1 34.9,78.1 34.5,78 27.7,107 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="20.8,104.4 34.9,78.1 34.5,77.9 20.4,104.2 						"/>
                            </g>
                            <g>

                                <rect x="9.6" y="88.7" transform="matrix(0.6838 -0.7297 0.7297 0.6838 -57.1303 45.9757)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>
                                <polygon class="st2" points="9.5,93.9 34.8,78.2 34.6,77.9 9.2,93.6 						"/>
                            </g>
                            <g>
                                <rect x="5.5" y="82.1" transform="matrix(0.957 -0.29 0.29 0.957 -23.007 9.4584)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="4.9" y="78.3" transform="matrix(0.9995 -3.248760e-02 3.248760e-02 0.9995 -2.5401 0.6838)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="20" y="59.7" transform="matrix(0.2264 -0.974 0.974 0.2264 -57.1035 77.3686)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="34.6,78.2 34.8,77.8 8.5,63.8 8.3,64.1 						"/>
                            </g>
                            <g>

                                <rect x="23.6" y="52.9" transform="matrix(0.6836 -0.7299 0.7299 0.6836 -41.9783 38.832)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="34.5,78.1 34.8,77.9 19.1,52.6 18.8,52.8 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="34.5,78.1 34.9,78 26.2,49.4 25.8,49.5 						"/>
                            </g>
                        </g>
                        <g class="roue roue-devant">
                            <g>
                                <polygon class="st2" points="138.7,77.7 139.1,77.6 138.2,47.8 137.8,47.8 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="139.1,77.7 145.9,48.7 145.5,48.6 138.7,77.6 						"/>
                            </g>
                            <g>

                                <rect x="131" y="64.3" transform="matrix(0.4716 -0.8818 0.8818 0.4716 20.2464 162.7831)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="134.2" y="66.6" transform="matrix(0.6838 -0.7297 0.7297 0.6838 -1.5631 129.9244)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="136.7" y="69.6" transform="matrix(0.8493 -0.5279 0.5279 0.8493 -13.9916 90.5299)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="138.3" y="73.1" transform="matrix(0.957 -0.29 0.29 0.957 -14.6809 47.5846)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="138.9" y="77" transform="matrix(0.9995 -3.242394e-02 3.242394e-02 0.9995 -2.421 5.0282)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="153.2" y="66.1" transform="matrix(0.2264 -0.974 0.974 0.2264 39.7714 212.1437)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>

                                <rect x="151.9" y="69.8" transform="matrix(0.4725 -0.8813 0.8813 0.4725 5.5908 178.6916)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="160.5,98.2 160.8,97.9 139.1,77.5 138.8,77.8 						"/>
                            </g>
                            <g>

                                <rect x="146.6" y="75.4" transform="matrix(0.8502 -0.5264 0.5264 0.8502 -25.553 90.7961)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="147.4,106.2 147.8,106.1 139.1,77.6 138.7,77.7 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="139.7,107.5 140.1,107.4 139.1,77.6 138.7,77.7 						"/>
                            </g>
                            <g>
                                <polygon class="st2" points="132.3,106.7 139.1,77.7 138.7,77.6 132,106.6 						"/>
                            </g>
                            <g>

                                <rect x="117" y="90.6" transform="matrix(0.4716 -0.8818 0.8818 0.4716 -10.3695 164.2757)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="113.8" y="88.3" transform="matrix(0.6838 -0.7297 0.7297 0.6838 -23.8881 121.9229)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="111.4" y="85.3" transform="matrix(0.8494 -0.5278 0.5278 0.8494 -26.1153 79.5271)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="109.7" y="81.8" transform="matrix(0.957 -0.29 0.29 0.957 -18.4154 39.6777)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>

                                <rect x="109.1" y="77.9" transform="matrix(0.9995 -3.248662e-02 3.248662e-02 0.9995 -2.4726 4.0703)" class="st2" width="29.8" height="0.4"/>
                            </g>
                            <g>
                                <polygon class="st2" points="138.9,77.8 139,77.4 109.9,70.7 109.8,71.1 						"/>
                            </g>
                            <g>

                                <rect x="125.6" y="55.7" transform="matrix(0.4722 -0.8815 0.8815 0.4722 4.1445 148.1462)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="138.8,77.8 139.1,77.5 117.3,57.1 117,57.4 						"/>
                            </g>
                            <g>

                                <rect x="130.9" y="50.1" transform="matrix(0.8496 -0.5274 0.5274 0.8496 -14.5635 78.883)" class="st2" width="0.4" height="29.8"/>
                            </g>
                            <g>
                                <polygon class="st2" points="138.7,77.7 139.1,77.6 130.5,49 130.1,49.2 						"/>
                            </g>
                        </g>
                        <g>
                            <path d="M30.7,78.2c0,0.2,0.1,0.4,0.2,0.5c0.1,0.2,0.2,0.3,0.2,0.5c0.1,0.1,0.2,0.3,0.3,0.4l0.2,0.2c0.1,0.1,0.1,0.1,0.2,0.2
						l0.2,0.2c0.1,0.1,0.1,0.1,0.2,0.2c0.2,0.1,0.3,0.2,0.5,0.3l0.5,0.2l0,0l0,0l0,0v0v0v0l0,0l0,0l-0.1,0l0,0l0,0l0,0l0,0l0,0
						c3.5,1.2,7.1,2.2,10.7,3.3l5.4,1.6l5.4,1.6c3.6,1,7.2,2,10.8,2.9c1.8,0.5,3.6,0.9,5.4,1.2c0.9,0.2,1.8,0.3,2.7,0.5
						c0.5,0.1,0.9,0.1,1.4,0.1c0.4,0,0.9,0,1.3-0.2l-0.1,0.1l0,0l0-0.1l0.1,0c1.4-0.1,2.8-0.6,4.1-1.3c1.2-0.8,2.3-1.8,3-3
						c0.7-1.2,1.2-2.6,1.3-4.1c0.1-1.4-0.1-2.9-0.7-4.2c-0.6-1.3-1.5-2.5-2.6-3.5c-1.1-0.9-2.4-1.6-3.9-1.9
						c-0.4-0.1-0.7-0.1-1.1-0.2c-0.4,0-0.7,0-1.1,0c-0.7,0-1.5,0-2.2,0l-4.4,0.1l-8.8,0.2L51.4,74l-8.8,0.1c-1.5,0-2.9,0.1-4.4,0.1
						c-1.5,0-2.9,0.1-4.4,0.2h0l-0.3,0l-0.3,0c-0.2,0-0.4,0.1-0.6,0.2c-0.2,0.1-0.4,0.2-0.5,0.3l-0.2,0.2l-0.2,0.2l-0.2,0.2
						l-0.2,0.2c-0.1,0.2-0.2,0.4-0.3,0.5c-0.1,0.2-0.1,0.4-0.2,0.6l0,0.3l0,0.3L30.7,78.2z M30.6,77.4l0-0.3c0-0.2,0.1-0.4,0.2-0.6
						c0.1-0.2,0.2-0.4,0.3-0.6l0.2-0.3l0.2-0.2l0.2-0.2L32,75c0.2-0.1,0.4-0.2,0.6-0.3c0.2-0.1,0.4-0.1,0.6-0.2l0.3,0l0.3,0l0,0
						c1.5-0.1,2.9-0.3,4.4-0.3c1.5-0.1,2.9-0.1,4.4-0.2l4.4-0.2l4.4-0.1c1.5,0,2.9-0.1,4.4-0.1l4.4-0.1l8.8-0.1l4.4,0
						c0.7,0,1.5,0,2.2,0c0.4,0,0.7,0,1.1,0.1c0.4,0.1,0.7,0.1,1.1,0.2c1.4,0.3,2.8,1,3.9,1.9c1.1,1,2,2.2,2.6,3.5
						c0.6,1.4,0.8,2.9,0.7,4.3c-0.1,1.5-0.5,2.9-1.3,4.2c-0.7,1.3-1.8,2.4-3.1,3.2c-1.3,0.8-2.7,1.3-4.2,1.4l0.2-0.2l0,0l0,0.1
						l-0.1,0c-0.5,0.2-1,0.2-1.5,0.2c-0.5,0-0.9-0.1-1.4-0.1c-0.9-0.1-1.9-0.3-2.8-0.4c-1.8-0.4-3.7-0.8-5.5-1.2
						c-3.6-0.9-7.2-1.9-10.8-3c-1.8-0.5-3.6-1.1-5.4-1.6c-1.8-0.5-3.6-1.1-5.3-1.7c-3.5-1.1-7.1-2.3-10.6-3.5l0,0l0,0l0,0l0,0
						l-0.1,0l0,0l0,0v0v0l0,0l0,0l0,0l-0.5-0.2c-0.2-0.1-0.3-0.2-0.5-0.3c-0.1,0-0.2-0.1-0.2-0.2l-0.2-0.2c-0.1-0.1-0.1-0.1-0.2-0.2
						l-0.2-0.2c-0.1-0.1-0.2-0.3-0.3-0.5c-0.1-0.2-0.2-0.3-0.2-0.5c0-0.2-0.1-0.4-0.2-0.5l0-0.6L30.6,77.4z"/>
                        </g>
                        <g>
                            <path class="st3" d="M119.6,16.3l9.3,0.5v0c0,0,0,0,0,0c0.3,0,0.6,0.3,0.5,1.2c-0.1,0.8-0.4,1.1-0.7,1.1l0,0h0c0,0,0,0,0,0
						c0,0,0,0,0,0l-7.1-0.4l2,7l-2.3,0.6l-2.6-9.1C118.5,16.4,119.6,16.3,119.6,16.3z"/>
                        </g>
                        <g>
                            <g>
                                <path class="st4" d="M119,16.5c0,0.1,0,0.1,0,0.2l2.6,9.1l1.9-0.5l0.1,0.4l-2.3,0.6l-2.6-9.1C118.6,16.9,118.8,16.7,119,16.5z
							"/>
                            </g>
                        </g>
                        <g>
                            <path class="st3" d="M126.7,18.1c0.4,0.1,0.8-0.2,1-0.6l0.6-2.2c0.1-0.4-0.1-0.9-0.6-1l-0.5-0.1c-0.4-0.1-0.8,0.2-1,0.6
						l-0.6,2.2c-0.1,0.4,0.1,0.9,0.6,1L126.7,18.1z"/>
                        </g>
                        <g>
                            <rect x="59.2" y="6" transform="matrix(0.978 -0.2086 0.2086 0.978 -2.1966 12.9797)" class="st4" width="2.4" height="21.7"/>
                        </g>
                        <g>

                            <rect x="59.9" y="6.1" transform="matrix(0.9781 -0.2082 0.2082 0.9781 -2.1871 13.0223)" class="st3" width="1.7" height="21.6"/>
                        </g>
                        <g class="vitesse">
                            <g>
                                <path class="st4" d="M75.9,92.1c0.5,0,0.9,0.2,1.1,0.6c0.2,0,0.3-0.1,0.5-0.1c0-0.4,0.3-0.8,0.8-1c0.5-0.2,1,0,1.2,0.3
							c0.2-0.1,0.3-0.1,0.4-0.2c-0.1-0.4,0.1-0.9,0.5-1.2c0.4-0.3,1-0.3,1.3,0c0.1-0.1,0.2-0.2,0.4-0.3c-0.2-0.4-0.1-0.9,0.2-1.3
							c0.4-0.4,0.9-0.5,1.3-0.3c0.1-0.1,0.2-0.3,0.3-0.4c-0.3-0.3-0.4-0.8-0.1-1.3c0.2-0.5,0.7-0.7,1.2-0.7c0.1-0.1,0.1-0.3,0.2-0.4
							c-0.4-0.2-0.6-0.7-0.5-1.2c0.1-0.5,0.5-0.9,1-0.9c0-0.2,0-0.3,0-0.5c-0.4-0.1-0.8-0.5-0.8-1.1c0-0.5,0.3-1,0.7-1.2
							c0-0.2-0.1-0.3-0.1-0.4c-0.4,0-0.9-0.3-1.1-0.8c-0.2-0.5,0-1,0.4-1.3c-0.1-0.1-0.1-0.3-0.2-0.4c-0.4,0.1-0.9-0.1-1.2-0.5
							c-0.3-0.5-0.3-1,0-1.3c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4,0.2-0.9,0.2-1.3-0.2c-0.4-0.4-0.5-0.9-0.3-1.3c-0.1-0.1-0.3-0.2-0.4-0.3
							c-0.3,0.3-0.8,0.4-1.3,0.2c-0.5-0.2-0.7-0.7-0.7-1.1c-0.1-0.1-0.3-0.1-0.5-0.2c-0.2,0.4-0.7,0.6-1.2,0.5
							c-0.5-0.1-0.9-0.5-0.9-0.9c-0.2,0-0.3,0-0.5,0c-0.1,0.4-0.5,0.7-1,0.8c-0.5,0-1-0.2-1.1-0.6c-0.2,0-0.3,0.1-0.5,0.1
							c0,0.4-0.3,0.8-0.8,1c-0.5,0.2-1,0-1.2-0.3c-0.2,0.1-0.3,0.1-0.5,0.2c0.1,0.4-0.1,0.9-0.5,1.1c-0.4,0.3-0.9,0.3-1.2,0.1
							c-0.1,0.1-0.3,0.2-0.4,0.3c0.2,0.4,0.1,0.8-0.2,1.2c-0.3,0.4-0.8,0.5-1.2,0.4c-0.1,0.1-0.2,0.3-0.3,0.4
							c0.2,0.3,0.3,0.8,0.1,1.2c-0.2,0.4-0.6,0.7-1,0.7c-0.1,0.2-0.1,0.3-0.2,0.5c0.3,0.2,0.5,0.7,0.4,1.1c-0.1,0.5-0.4,0.8-0.8,0.9
							c0,0.2,0,0.4,0,0.5c0.4,0.2,0.6,0.5,0.6,1c0,0.5-0.2,0.9-0.5,1.1c0,0.2,0.1,0.4,0.1,0.5c0.4,0.1,0.7,0.4,0.9,0.8
							c0.1,0.4,0,0.9-0.2,1.2c0.1,0.2,0.2,0.3,0.2,0.5c0.4,0,0.8,0.2,1.1,0.6c0.3,0.4,0.3,0.9,0.1,1.2c0.1,0.1,0.2,0.3,0.3,0.4
							c0.4-0.1,0.8-0.1,1.2,0.3c0.4,0.3,0.5,0.8,0.4,1.2c0.1,0.1,0.3,0.2,0.4,0.3c0.3-0.2,0.8-0.3,1.2-0.1c0.4,0.2,0.7,0.6,0.7,1
							c0.2,0.1,0.3,0.1,0.5,0.2c0.2-0.3,0.7-0.5,1.2-0.4c0.5,0.1,0.8,0.4,0.9,0.8c0.2,0,0.3,0,0.5,0C75,92.4,75.4,92.2,75.9,92.1z
							 M68.8,82.3l4.3,0.7c0,0.1,0.1,0.2,0.1,0.4l-3,3.1C69.3,85.3,68.8,83.9,68.8,82.3z M72.2,76.7l1.5,4.1
							c-0.1,0.1-0.3,0.3-0.4,0.5l-4.3-0.7C69.6,78.9,70.8,77.5,72.2,76.7z M76.6,76c0.8,0.2,1.5,0.5,2.2,0.9L76,80.2
							c-0.1,0-0.1,0-0.2-0.1c-0.1,0-0.3,0-0.4,0L73.9,76C74.8,75.8,75.7,75.8,76.6,76z M81.8,82.8l-4.3-0.9c0-0.2-0.1-0.4-0.2-0.6
							l2.8-3.3C81.3,79.3,81.9,81,81.8,82.8z M78.1,88.3l-1.3-4.2c0.2-0.1,0.3-0.3,0.4-0.5l4.2,0.9C80.9,86.2,79.7,87.5,78.1,88.3z
							 M74,88.8c-0.9-0.2-1.8-0.6-2.5-1.1l3-3.1c0.1,0,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3,0l1.3,4.1C75.7,88.9,74.9,88.9,74,88.8z"/>
                            </g>
                        </g>
                        <g>
                            <path class="st5" d="M104,77.6c0,19.3,15.6,34.9,34.9,34.9c19.3,0,34.9-15.6,34.9-34.9c0-19.3-15.6-34.9-34.9-34.9
						C119.6,42.7,104,58.4,104,77.6z M108.6,77.6c0-16.7,13.6-30.3,30.3-30.3s30.3,13.6,30.3,30.3s-13.6,30.3-30.3,30.3
						S108.6,94.4,108.6,77.6z"/>
                        </g>
                        <g>
                            <path class="st6" d="M107.1,77.6c0,17.6,14.3,31.9,31.9,31.9c17.6,0,31.9-14.3,31.9-31.9c0-17.6-14.3-31.9-31.9-31.9
						C121.3,45.8,107.1,60,107.1,77.6z M109.5,77.6c0-16.2,13.2-29.4,29.4-29.4s29.4,13.2,29.4,29.4c0,16.2-13.2,29.4-29.4,29.4
						S109.5,93.9,109.5,77.6z"/>
                        </g>
                        <g>
                            <path class="st4" d="M108.7,77.6c0,16.7,13.5,30.2,30.2,30.2c16.7,0,30.2-13.5,30.2-30.2c0-16.7-13.5-30.2-30.2-30.2
						C122.2,47.4,108.7,60.9,108.7,77.6z M109.5,77.6c0-16.2,13.2-29.4,29.4-29.4s29.4,13.2,29.4,29.4c0,16.2-13.2,29.4-29.4,29.4
						S109.5,93.9,109.5,77.6z"/>
                        </g>
                        <g>
                            <path class="st5" d="M0,77.6c0,19.3,15.6,34.9,34.9,34.9c19.3,0,34.9-15.6,34.9-34.9c0-19.3-15.6-34.9-34.9-34.9
						C15.6,42.7,0,58.4,0,77.6z M4.6,77.6c0-16.7,13.6-30.3,30.3-30.3c16.7,0,30.3,13.6,30.3,30.3s-13.6,30.3-30.3,30.3
						C18.2,107.9,4.6,94.4,4.6,77.6z"/>
                        </g>
                        <g>
                            <path class="st6" d="M3,77.6c0,17.6,14.3,31.9,31.9,31.9c17.6,0,31.9-14.3,31.9-31.9c0-17.6-14.3-31.9-31.9-31.9
						C17.3,45.8,3,60,3,77.6z M5.5,77.6c0-16.2,13.2-29.4,29.4-29.4c16.2,0,29.4,13.2,29.4,29.4c0,16.2-13.2,29.4-29.4,29.4
						C18.7,107,5.5,93.9,5.5,77.6z"/>
                        </g>
                        <g>
                            <path class="st4" d="M4.7,77.6c0,16.7,13.5,30.2,30.2,30.2c16.7,0,30.2-13.5,30.2-30.2c0-16.7-13.5-30.2-30.2-30.2
						C18.2,47.4,4.7,60.9,4.7,77.6z M5.5,77.6c0-16.2,13.2-29.4,29.4-29.4c16.2,0,29.4,13.2,29.4,29.4c0,16.2-13.2,29.4-29.4,29.4
						C18.7,107,5.5,93.9,5.5,77.6z"/>
                        </g>
                        <g>
                            <circle class="st4" cx="33.8" cy="77.6" r="3.2"/>
                        </g>
                        <g>
                            <circle class="st4" cx="139.2" cy="77.4" r="3.1"/>
                        </g>
                        <g>
                            <rect x="63" y="25.9" class="st7" width="60.1" height="2.9"/>
                        </g>
                        <g>
                            <rect x="63" y="27.9" class="st8" width="60.1" height="0.9"/>
                        </g>
                        <g>

                            <rect x="66.8" y="23.9" transform="matrix(0.978 -0.2087 0.2087 0.978 -9.7673 15.4368)" class="st7" width="2.9" height="60.1"/>
                        </g>
                        <g>
                            <polygon class="st8" points="61.8,24.6 74.2,82.9 75.9,82.5 76,83.1 73.1,83.7 60.6,24.9 					"/>
                        </g>
                        <g>
                            <polygon class="st7" points="34.8,78.3 61.6,29.7 60.7,25.7 33.5,77.5 					"/>
                        </g>
                        <g>
                            <polygon class="st7" points="33.8,78.1 74.6,84 75,81 34,76.5 					"/>
                        </g>
                        <g>

                            <rect x="65.7" y="59.3" transform="matrix(0.7484 -0.6633 0.6633 0.7484 -15.2207 81.4755)" class="st7" width="68.2" height="2.9"/>
                        </g>
                        <g>
                            <polygon class="st8" points="125.8,38.7 124.3,37.1 124.4,37.1 126.3,39.3 75.2,84.5 74.7,84 					"/>
                        </g>
                        <g>
                            <path class="st7" d="M120.9,25.7l2.8-0.7l4.4,17.4l0,0c2.4,11.8,7.5,26.8,11.6,33.6c0.4,0.7,0.8,1.3,0.4,1.9
						c-0.3,0.4-0.7,0.6-1.2,0.6c-0.3,0-0.6,0-0.7-0.2c-5.9-6.5-11.8-30-13-35.2h0L120.9,25.7z"/>
                        </g>
                        <g>
                            <path class="st8" d="M123.7,25l0.1,0.2l-1.9,0.5l4.4,17.4h0c1.2,5.2,7.1,28.7,13,35.2c0.1,0.1,0.1,0.1,0.2,0.1
						c-0.1,0-0.3,0.1-0.4,0.1c-0.3,0-0.6,0-0.7-0.2c-5.9-6.5-11.8-30-13-35.2h0l-4.4-17.4L123.7,25z"/>
                        </g>
                        <g>
                            <path class="st9" d="M71.2,0.7c0,0-9,0.7-15.4,0C49.5,0,49.3,0,48.9,0c-0.8,0-3.3,0.1-2.3,2.6C47.6,5.1,51.7,8,55,9
						c3.3,0.9,7.7-1.1,8.5-1.8c0.9-0.7,4.1-2.4,9.7-1.6c1.3,0.1,2.2-1.3,2.1-2.5C75.2,1.7,74.7,0.5,71.2,0.7z"/>
                        </g>
                        <g class="st10">
                            <path class="st5" d="M47.1,0.4c0,0.2,0.1,0.4,0.2,0.6c1,2.5,5.1,5.4,8.4,6.3c3.3,0.9,7.7-1.1,8.5-1.8c0.9-0.7,4.1-2.4,9.7-1.6
						C74.5,4,75,3.8,75.3,3.4c-0.1,1.1-0.9,2.3-2.1,2.2c-5.6-0.8-8.8,0.9-9.7,1.6C62.7,7.8,58.3,9.9,55,9c-3.3-0.9-7.4-3.9-8.4-6.3
						C46.1,1.4,46.5,0.8,47.1,0.4z"/>
                        </g>
                        <g>
                            <g>
                                <g>
                                    <path class="st11" d="M87.5,97c0.5,0.5,1.2,0.5,1.7,0l0,0c0.5-0.5,0.5-1.2,0-1.7L75.7,81.6c-0.5-0.5-1.2-0.5-1.7,0l0,0
								c-0.5,0.5-0.5,1.2,0,1.7L87.5,97z"/>
                                </g>
                            </g>
                            <g>
                                <g>

                                    <ellipse transform="matrix(0.2002 -0.9798 0.9798 0.2002 -20.9461 139.9298)" class="st2" cx="75.2" cy="82.8" rx="0.8" ry="0.8"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st4" d="M94.1,95.7c-0.3-1.2-1.6-2-2.8-1.6l-5.4,1.4c-1.2,0.3-2,1.6-1.6,2.8l0,0c0.3,1.2,1.6,2,2.8,1.6l5.4-1.4
								C93.7,98.2,94.4,97,94.1,95.7L94.1,95.7z"/>
                                </g>
                            </g>
                        </g>
                        <g>
                            <path class="st11" d="M120.1,25.2c0.1,0.4,0.5,0.7,1,0.6l2.2-0.5c0.4-0.1,0.7-0.5,0.6-1l-0.1-0.5c-0.1-0.4-0.5-0.7-1-0.6
						l-2.2,0.5c-0.4,0.1-0.7,0.5-0.6,1L120.1,25.2z"/>
                        </g>
                        <g>
                            <path class="st11" d="M125,43.4c0.1,0.4,0.5,0.7,1,0.6l2.2-0.5c0.4-0.1,0.7-0.5,0.6-1l-0.1-0.6c-0.1-0.4-0.5-0.7-1-0.6
						l-2.2,0.5c-0.4,0.1-0.7,0.5-0.6,1L125,43.4z"/>
                        </g>
                        <g>
                            <g>
                                <path class="st5" d="M128.2,33.2c9,0,14.6-3.5,14.6-9.2c0-6.5-7.7-10.3-15.4-11.1c-0.8-0.1-1.5,0.5-1.5,1.2
							c-0.1,0.8,0.5,1.5,1.2,1.5c6.4,0.7,12.9,3.8,12.9,8.3c0,6-9,6.4-11.7,6.4c-0.8,0-1.4,0.6-1.4,1.4
							C126.8,32.6,127.4,33.2,128.2,33.2z"/>
                            </g>
                        </g>
                        <g class="st10">
                            <g>
                                <path class="st12" d="M59.2,9.2c0,0,0,0.2,0.1,0.7c0.1,0.4,0.2,1,0.3,1.7c0.1,0.7,0.3,1.5,0.4,2.4c0.2,0.9,0.4,1.8,0.6,2.8
							c0.2,0.9,0.4,1.9,0.6,2.8c0.2,0.9,0.4,1.7,0.6,2.4c0.2,0.7,0.3,1.3,0.4,1.7c0.1,0.4,0.2,0.6,0.2,0.6s0-0.2-0.1-0.7
							c-0.1-0.4-0.2-1-0.3-1.7c-0.1-0.7-0.3-1.5-0.4-2.4c-0.2-0.9-0.4-1.8-0.6-2.8c-0.2-0.9-0.4-1.9-0.6-2.8
							c-0.2-0.9-0.4-1.7-0.6-2.4c-0.2-0.7-0.3-1.3-0.4-1.7C59.3,9.4,59.2,9.2,59.2,9.2z"/>
                            </g>
                        </g>
                        <g>
                            <circle class="st2" cx="33.8" cy="77.6" r="1.3"/>
                        </g>
                        <g>
                            <circle class="st2" cx="89.2" cy="97" r="1.3"/>
                        </g>
                        <g>
                            <circle class="st2" cx="139.3" cy="77.2" r="1.3"/>
                        </g>
                        <g class="st13">
                            <g>
                                <path class="st12" d="M86.6,93.8c0,0-0.1-0.1-0.4-0.4c-0.2-0.3-0.6-0.6-1-1.1c-0.4-0.4-0.9-0.9-1.4-1.5
							c-0.5-0.5-1.1-1.1-1.6-1.7c-0.6-0.6-1.1-1.1-1.7-1.6c-0.5-0.5-1.1-1-1.5-1.4c-0.4-0.4-0.8-0.7-1.1-1c-0.3-0.2-0.4-0.4-0.4-0.4
							s0.1,0.1,0.4,0.4c0.2,0.3,0.6,0.6,1,1.1c0.4,0.4,0.9,0.9,1.4,1.5c0.5,0.5,1.1,1.1,1.6,1.7c0.6,0.6,1.1,1.1,1.7,1.6
							c0.5,0.5,1.1,1,1.5,1.4c0.4,0.4,0.8,0.7,1.1,1C86.4,93.7,86.6,93.8,86.6,93.8z"/>
                            </g>
                        </g>
                        <g class="st10">
                            <g>
                                <path class="st12" d="M64.3,31.7c0,0,0.1,0.7,0.3,1.8c0.2,1.2,0.5,2.8,0.8,4.8c0.4,2,0.8,4.3,1.3,6.7c0.2,1.2,0.5,2.5,0.8,3.8
							c0.3,1.3,0.5,2.6,0.8,3.9c0.3,1.3,0.6,2.6,0.8,3.9c0.3,1.3,0.6,2.5,0.8,3.8c0.5,2.4,1.1,4.7,1.5,6.7c0.5,1.9,0.9,3.6,1.1,4.7
							c0.3,1.1,0.5,1.8,0.5,1.8s-0.1-0.7-0.3-1.8c-0.2-1.1-0.5-2.8-0.8-4.8c-0.4-2-0.8-4.3-1.3-6.7c-0.2-1.2-0.5-2.5-0.8-3.8
							c-0.3-1.3-0.5-2.6-0.8-3.9c-0.3-1.3-0.6-2.6-0.8-3.9c-0.3-1.3-0.6-2.5-0.8-3.8c-0.5-2.4-1.1-4.7-1.5-6.7
							c-0.5-1.9-0.9-3.6-1.1-4.7C64.5,32.4,64.3,31.7,64.3,31.7z"/>
                            </g>
                        </g>
                        <g class="st10">
                            <g>
                                <path class="st12" d="M116.7,26.8c0,0-0.8,0-2.1-0.1c-1.3,0-3.2-0.1-5.4-0.2c-2.3-0.1-4.9-0.1-7.7-0.1c-1.4,0-2.9,0-4.3,0
							c-1.5,0-3,0-4.5,0c-1.5,0-3,0-4.5,0c-1.5,0-2.9,0-4.3,0c-2.8,0-5.4,0.1-7.7,0.1c-2.3,0-4.1,0.1-5.4,0.2
							c-1.3,0.1-2.1,0.1-2.1,0.1s0.8,0,2.1,0.1c1.3,0,3.2,0.1,5.4,0.2c2.3,0.1,4.9,0.1,7.7,0.1c1.4,0,2.9,0,4.3,0c1.5,0,3,0,4.5,0
							c1.5,0,3,0,4.5,0c1.5,0,2.9,0,4.3,0c2.8,0,5.4-0.1,7.7-0.1c2.3,0,4.1-0.1,5.4-0.2C116,26.8,116.7,26.8,116.7,26.8z"/>
                            </g>
                        </g>
                        <g class="st10">
                            <g>
                                <path class="st12" d="M55.8,37.5c0,0-0.2,0.4-0.7,1.2c-0.4,0.8-1,1.8-1.7,3.1c-0.7,1.3-1.5,2.8-2.4,4.4
							c-0.9,1.6-1.8,3.4-2.7,5.1c-0.9,1.7-1.8,3.5-2.7,5.1c-0.9,1.6-1.6,3.2-2.3,4.5c-0.7,1.3-1.2,2.4-1.6,3.2
							c-0.4,0.8-0.6,1.2-0.6,1.2s0.2-0.4,0.7-1.2c0.4-0.8,1-1.8,1.7-3.1c0.7-1.3,1.5-2.8,2.4-4.4c0.9-1.6,1.8-3.4,2.7-5.1
							c0.9-1.7,1.8-3.5,2.7-5.1c0.9-1.6,1.6-3.2,2.3-4.5c0.7-1.3,1.2-2.4,1.6-3.2C55.5,38,55.8,37.5,55.8,37.5z"/>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
        </g>
</svg>
    <span class="chargement">Chargement</span>
</div>

<header class="header">
	<div class="wrap-flex">
		<nav class="header__nav header__nav--left">
			<a class="active" href="#">Accueil</a>
			<a href="#">En cours</a>
			<a href="#">Historique</a>
		</nav>
		<span id="header__burger"></span>
		<nav class="header__nav header__nav--right">
			<a class="header__nav--account" href="#">Sophia Lopez</a>
			<a href="#">Deconnexion</a>
		</nav>
</header>

<main>
	<section class="feature">
		<article class="wrap">
			<h1 class="feature__title">Nom du modèle</h1>
			<p class="feature__content">Taille S - Automatique</p>
			<p class="feature__color">Vert Anglais</p>
			<a class='btn-primary btn ' href="#">commander</a>
		</article>
	</section>

	<section class="best-seller wrap">
		<header>
			<span class="titre2">Best seller</span>
			<form action="#">
				<label for="tri">trier par :</label>
				<div class="select__custom select__general">
					<span class="select__icon-choose"></span>

					<select name="tri" id="tri">
						<option data-tri="price" value="valeur1">Prix croissant</option>
						<option data-tri="stock" value="valeur2" selected>stock</option>
						<option data-tri="priceDesc" value="valeur3">Prix décroissant</option>
						<option data-tri="name" value="valeur3">Trier par nom</option>

					</select>
				</div>
			</form>
		</header>
		<div class="best-seller__list">
			<ul id="slider" class="best-seller__slide">
			</ul>
		</div>
	</section>

	<section class="nouveaute">
		<div class="wrap">
			<strong class="titre3">Nouveautés</strong>
			<div class="nouveaute__slider-nouveaute">
				<div data-action="move" class="nouveaute__slider--arrow nouveaute__slider--arrow-left"></div>
				<div data-action="move" class="nouveaute__slider--arrow nouveaute__slider--arrow-right"></div>
			</div>
			<div class="nouveaute__dots">
			</div>
		</div>
	</section>

	<section class="information">
		<div class="wrap-flex">
			<div class="information__col-f2 information__col-historique">
				<span class="titre2">Historique de vos commandes</span>
				<ul class="information__list">
					<li class="information__item">
						<article class="information__article">
							<figure class="information__image-client information__image-client-warning">
								<img src="images/client.png" alt="">
								<span class="information__image-client-icon information__image-client-icon-warning"></span>
								<figcaption>Attention requise</figcaption>
							</figure>
							<header>
								<h1>Commande N°365412</h1>
								<span>Par <strong>Vous</strong> le <time datetime="2015-02-02"><strong>02 février 2015</strong></time></span>
								<span class="information__article-state-warning">attention requise</span>

							</header>
							<a class="btn btn-command btn-danger" href="#">consulter</a>
						</article>
					</li>
					<li class="information__item">
						<article class="information__article">
							<figure class="information__image-client information__image-client-general">
								<img src="images/client2.png" alt="">
								<span class="information__image-client-icon information__image-client-icon-transi"></span>
								<figcaption>En livraison</figcaption>
							</figure>
							<header>
								<h1>Commande N°365348</h1>
								<span>Par <strong>Pierre Amiro</strong> le <time datetime="2015-01-22"><strong>22 janvier 2015</strong></time></span>
								<span class="information__article-state-transit">en cours d’expédition</span>
							</header>
							<a class="btn btn-command btn-wait" href="#">consulter</a>

						</article>
					</li>
					<li class="information__item">
						<article class="information__article">
							<figure class="information__image-client information__image-client-general">
								<img src="images/client.png" alt="">
								<span class="information__image-client-icon information__image-client-icon-deliver"></span>
								<figcaption>En livraison</figcaption>
							</figure>
							<header>
								<h1>Commande N°359836</h1>
								<span>Par <strong>Vous</strong> le <time datetime="2015-01-06"><strong>06 janvier 2015</strong></time></span>
								<span class="information__article-state-deliver">receptionnée</span>
							</header>
							<a class="btn btn-command btn-deliver" href="#">consulter</a>

						</article>
					</li>
					<li class="information__item">
						<article class="information__article">
							<figure class="information__image-client information__image-client-general">
								<img src="images/client2.png" alt="">
								<span class="information__image-client-icon information__image-client-icon-deliver"></span>
								<figcaption>En livraison</figcaption>
							</figure>
							<header>
								<h1>Commande N°359134</h1>
								<span>Par <strong>Pierre Amiro</strong> le <time datetime="2015-12-22"><strong>22 décembre 2015</strong></time></span>
								<span class="information__article-state-deliver">receptionnée</span>
							</header>
							<a class="btn btn-command btn-deliver" href="#">consulter</a>

						</article>
					</li>
				</ul>
				<div class="information__col-historique-all">
					<a href="#" class="btn btn-all-historique">voir l'historique</a>
				</div>
			</div>
			<div class="information__col-f2 information__col-formations">
				<span class="titre2">Formations liés</span>
				<ul class="information__list">
					<li class="information__item">
						<article class="information__formations-artcile">
							<figure class="information__image-client">
								<img src="images/client_formation.png" alt="">
							</figure>
							<header>
								<span>force de vente</span>
								<h1>Optimiser vos campagnes pulicitaires</h1>
								<span class="information__col-formations--recommend">recommandée</span>
							</header>
						</article>
					</li>
                    <li class="information__item">
						<article>
							<figure class="information__image-client information__image-client-general">
								<img src="images/client_formation.png" alt="">
							</figure>
							<header>
								<span>force de vente</span>
								<h1>Gestion des stocks</h1>
								<span class="information__col-formations--recommend">recommandée</span>
							</header>
						</article>
					</li>

				</ul>
			</div>
		</div>
	</section>

	<section class="ventes">
		<div class="wrap">
			<header>
                    <span class="titre2">
                        Ventes / Commandes
                    </span>
				<form action="#">

					<div class="form-group">
						<label for="modele">modèle :</label>
						<div class="select__custom select__general">
							<span class="select__icon-choose"></span>

							<select name="tri">
								<option value="valeur1">Valeur 1</option>
								<option value="valeur2" selected="">Nom du modele</option>
								<option value="valeur3">Valeur 3</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="select__custom select__general">
							<span class="select__icon-choose"></span>
							<label for="couleur">couleur : </label>
							<select name="couleur" id="couleur">
								<option value="valeur1">Valeur 1</option>
								<option value="valeur2" selected>Noir</option>
								<option value="valeur3">Valeur 3</option>
							</select>
						</div>
					</div>
				</form>
			</header>
			<span class="previous-model"></span>
			<canvas id="canvas" width="824" height="290">
			</canvas>
		</div>
	</section>

	<section class="all-model best-seller wrap">
		<header>
			<span class="titre2">Tous les modèles</span>
			<form action="#">
				<div class="form-group">
					<span class="select__icon-glass"></span>
					<input id="search" type="search" placeholder="Rechercher un modèle">
				</div>
				<div class="form-group">
					<label for="tri">trier par :</label>
					<div class="select__custom select__general">
						<span class="select__icon-choose"></span>
						<select name="tri" id="tri">
							<option value="valeur1" data-tri="prix">Prix -</option>
							<option value="valeur1" data-tri="name">Trier par nom</option>
							<option value="valeur1" selected data-tri="stock">stock</option>
							<option value="valeur2" data-tri="popularite">popularité</option>
							<option value="valeur3" data-tri="priceDesc">Prix +</option>
						</select>
					</div>
				</div>

			</form>
		</header>

		<div class="best-seller__list">
			<ul id="all-model__slide" class="best-seller__slide">

			</ul>
			<div class="best-seller__see">
				<a class="btn btn-modele" href="#">voir tous les modèles</a>
			</div>
		</div>
	</section>

</main>

<footer class="footer-page">
	<section class="wrap-flex">
		<div>
			<strong class="titre3" >Navigation</strong>
			<ul>
				<li>
					<a href="#">actualités</a>
				</li>
				<li>
					<a href="#">annuaire</a>
				</li>
				<li>
					<a href="#">app store</a>
				</li>
				<li>
					<a href="#">commandes</a>
				</li>
				<li>
					<a href="#">formations</a>
				</li>
				<li>
					<a href="#">publicités</a>
				</li>
			</ul>
		</div>
		<div>
			<strong class="titre3" >Mon compte</strong>
			<ul>
				<li>
					<a href="#">contacts favoris</a>
				</li>
				<li>
					<a href="#">commandes</a>
				</li>
				<li>
					<a href="#">formations</a>
				</li>
				<li>
					<a href="#">préférences</a>
				</li>
				<li>
					<a href="#">profil</a>
				</li>
				<li>
					<a href="#">publicités</a>
				</li>

			</ul>
		</div>
		<div>
			<strong class="titre3" >Applications</strong>
			<ul>
				<li>
					<a href="#">Catalogue</a>
				</li>
				<li>
					<a href="#">App SAV</a>
				</li>
				<li>
					<a href="#">App Garantie</a>
				</li>
				<li>
					<a href="#">User Management</a>
				</li>
				<li>
					<a href="#">CRM</a>
				</li>
				<li>
					<a href="#">ERP</a>
				</li>

			</ul>
		</div>
		<div>
			<strong class="titre3" >Liens</strong>
			<ul>
				<li>
					<a href="#">à propos</a>
				</li>
				<li>
					<a href="#">conditions d’utilisation</a>
				</li>
				<li>
					<a href="#">mentions légales</a>
				</li>
				<li>
					<a href="#">intranet</a>
				</li>
				<li>
					<a href="#">site.com</a>
				</li>
			</ul>
		</div>

	</section>
</footer>
<script src="scripts/BezierEasing.js"></script>
<script src="scripts/ScrollMagic.min.js"></script>
<script src="scripts/animation.gsap.min.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>