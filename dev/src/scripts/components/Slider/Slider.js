import BUS from './BUS';
import SliderItem from './SliderItem';
import SliderBullet from './SliderBullet';
import SliderBullets from './SliderBullets';

// Création de la class Slider
export default class Slider {
    // Déclaration des variables
    // Peut être rajouter url
    constructor(el) {
        this.el = el;
        this.items = [];
        this.bullets = [];
        this.current = 0;
        this.max = 0;
    }

    // On passe l'élément à la création
    // Méthode pour initialiser
    init(url) {
        this.left = this.el.querySelector('.nouveaute__slider--arrow-left');
        this.right = this.el.querySelector('.nouveaute__slider--arrow-right');
        this.load(url);
    }
    // Chargement des données
    load(url) {
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.addEventListener('readystatechange', () => this.loaded(req));
        req.send();
    }
    loaded(req) {
        if (req.readyState == 4){
            if(req.status == 200){
                this.build(JSON.parse(req.responseText));
            }
        }
    }
    build(datas) {
        this.max = datas.length;
        var bullets = document.querySelector('.nouveaute__dots');

        var i = 0;
        var item;

        for(i; i < this.max; i++){
            item = new SliderItem(i, datas[i]);
            this.el.appendChild(item.build(i));
            this.items.push(item);
            var bullet = new SliderBullet(i, datas[i]);
            bullets.appendChild(bullet.build(i));
            this.bullets.push(bullet);

        }
        this.listen();
    }
    listen () {
        this.sliding = true;
        if (this.sliding) {
            this.right.addEventListener('click', () => this.move(+1));
            this.left.addEventListener('click', () => this.move(-1));
            BUS.listen('bulletMove', (e) => this.bulletMove(e));
        }
    }
    bulletMove(e) {
        this.move(e.detail.bullet - this.current);
    }
    move(dir) {

        if (this.sliding) {
            this.sliding = false;
            this.items[this.current].hide(dir);

            this.bullets[this.current].hide(dir);
            this.current = Math.max(-1, Math.min(this.max, this.current + dir));
            if (this.current === this.max) {
                this.current = 0;
            } else if(this.current === -1) {
                this.current = this.max - 1;
            }
            this.bullets[this.current].show(dir);
            this.items[this.current].show(dir);
        }
        setTimeout(() => {
            this.sliding = true;
        }, 3500);
    }
}
