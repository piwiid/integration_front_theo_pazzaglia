export default class SliderBullets {
    constructor(el) {
        this.el = el;
    }
    add(id, datas) {
        var bullet = new SliderBullet(id, datas);
        this.el.appendChild(bullet.build());
    }
}