import { animationBestSeller } from '../animation';

import bestSellerItem from './bestSellerItem';


export default class bestSeller {
    constructor(el) {
        this.el = el;
        this.items = [];
        this.tl = new TimelineMax();

    }

    init(url) {
        this.select = this.el.parentNode.parentNode.children[0].querySelector('select');
        this.selectAll = this.select.querySelectorAll('option');
        this.load(url);
        this.listen();
    }

    load(url) {
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.addEventListener('readystatechange', () => this.loaded(req));
        req.send();
        var item;
    }
    loaded(req) {
        if (req.readyState == 4){
            if(req.status == 200){
                this.build(JSON.parse(req.responseText));
            }
        }
    }
    build(blogPosts) {
        blogPosts.forEach((data,index) => {
            this.items.push(data);
            let item = new bestSellerItem(index, data);
            this.el.appendChild(item.build(index));
        });
    }

    // On écoute le select pour pouvoir trier
    listen() {
        // On écoute l'état du select
        this.select.addEventListener('change', () => this.sort());
    }
    searchItem(get) {
        // On écoute l'état sur l'input recherche
        get.addEventListener('change', (e) => this.afficherListe(e));
        get.addEventListener('keyup', (e) => this.afficherListe(e));
    }

    sort() {
        var tri = this.select[this.select.selectedIndex].getAttribute('data-tri');
        var sort = this.sortByProperty(this.items, tri);
        let el = this.el.querySelectorAll('li');
        // Animation des li
        el.forEach((item) => {
            TweenMax.to(item, 1, {
                delay: `${Math.random() * 0.5}`,
                autoAlpha:0,
                ease:new Ease(BezierEasing(0.77, 0, 0.175, 1)),
            });
        });
        // On enlève les anciens li et on remplace
        TweenMax.delayedCall(1.5, () => {
            this.el.querySelectorAll('li').forEach(e => e.parentNode.removeChild(e));
            this.items.forEach((data, index) => {
                let built = new bestSellerItem(index, data);
                this.el.appendChild(built.build(index));
            });
        });
    }

    sortByProperty(array, propertyName) {
        return array.sort(function (a, b) {
            switch (propertyName) {
                case "name": {
                    if(a[propertyName] > b[propertyName]) {
                        return 1;
                    } else {
                        return -1;
                    }
                    break;
                }
                case "stock": {
                    return a[propertyName] - b[propertyName];
                    break;
                }
                case "price": {
                    return a[propertyName] - b[propertyName];
                    break;
                }
                case "popularite": {
                    return a[propertyName] - b[propertyName];
                    break;
                }
                case "priceDesc": {
                    return a["price"] + b["price"];
                    break;
                }
            }
        });
    }
    searchModel (recherche,items) {
        return items.filter(item => {
            // Ce que l'on vient de taper va être cherche de façon général
            const REGEX = new RegExp(recherche, "gi")
            return item.name.match(REGEX) || item.category.match(REGEX);
        });
    }
    afficherListe(key) {
        const tableau = this.searchModel(key.target.value, this.items);
        const html = tableau.map(item => {
            return `
                    <li class="best-seller__item best-seller__item--search">
                        <article class="best-seller__item--article">
                            <div class="best-seller__item--image">
                                <img src="${item.img}" alt="#">
                            </div>
                            <h2 class="best-seller__item--title">${item.name}</h2>
                            <p class="best-seller__item--category">${item.category}</p>
                            <p class="best-seller__item--stock">${item.stock} en stock</p>
                        </article>
                    </li>
                    `;
        }).join('');

        this.el.innerHTML = html;
    }
}