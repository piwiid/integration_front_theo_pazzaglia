import { animationBestSeller } from '../animation';
export default class bestSellerItem {
    constructor(id, datas) {
        this.id = id;
        this.datas = datas;
        this.ani = new TimelineMax();
    }
    build(index) {
        this.el = document.createElement('li');
        this.el.classList.add('best-seller__item');

        if(this.datas.stock <= 2) {
            this.el.classList.add('best-seller__item--warning');
        }
        this.el.innerHTML = `
                    <article class="best-seller__item--article">
                        <div class="best-seller__item--image">
                            <img src="${this.datas.img}" alt="#">
                        </div>
                        <h2 class="best-seller__item--title">${this.datas.name}</h2>
                        <p class="best-seller__item--category">${this.datas.category}</p>
                        <p class="best-seller__item--stock">${this.datas.stock} en stock</p>
                    </article>`;
        animationBestSeller(this.el);
        return this.el;
    }

}