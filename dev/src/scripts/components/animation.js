export function animationBestSeller(el) {
    TweenMax.to(el, 1, {
        autoAlpha:1,
        ease:new Ease(BezierEasing(0.77, 0, 0.175, 1)),
        delay: `${Math.random() * 1}`,
    });
}

export function scrollBest (start, target) {
    var animationStart = document.querySelector(start);
    var fadeinBest = animationStart.querySelectorAll(target);
    var elementsArray = [];

    for (var i = 0; i < fadeinBest.length; ++i) {
        elementsArray.push(fadeinBest[i]);
    }

    var tl = new TimelineMax();
    tl.set(elementsArray, {x: "-50%",autoAlpha:0});

    function spotsIn(){
        var child = new TimelineMax ();
        child.staggerTo(elementsArray, 1, {
            x: "0%",
            autoAlpha: 1,
            ease: new Ease(BezierEasing(0.77, 0, 0.175, 1)),
        }, 0.2);
        //return the timeline
        return child;
    }


    var controller = new ScrollMagic.Controller();
    var fadein_scene = new ScrollMagic.Scene({
        triggerElement: target,
        reverse: false,
    })
        .setTween(spotsIn())
        .addTo(controller);
    /*fadein_scene.addIndicators();*/
}



