var canvas = document.getElementById("canvas");
var contexte = canvas.getContext('2d');

// Partie ligne
contexte.beginPath();
contexte.lineWidth = '1';
contexte.fillStyle = "#000";
var line = 0;
for(i = 1; i < 8; i++) {
    contexte.fillRect(0, line ,824,1);
    line = line + 30;
}
contexte.fill();

// Text canvas
contexte.font = "bold 12px Arial";
contexte.fillStyle = "white";
contexte.fillText("octobre",82,210);
contexte.fillText("novembre",228,210);
contexte.fillText("décembre",385,210);
contexte.fillText("janvier",547,210);
contexte.fillText("février",703,210);

contexte.font = "bold 9px Arial";
contexte.fillStyle = "black";
contexte.fillText("ventes",336,262);
contexte.fillText("commandes",396,262);
contexte.fillText("stock",480,262);

// Carré
contexte.fillStyle = "#e9e9e9";
contexte.fillRect(468,255, 8, 8);

// Partie courbe

contexte.lineWidth = "1";
contexte.beginPath();
contexte.moveTo(135,30);
contexte.quadraticCurveTo(214, -34, 290, 22);
contexte.quadraticCurveTo(350, 120, 408, 148);
contexte.quadraticCurveTo(420, 170, 545, 65);
contexte.quadraticCurveTo(594, 51, 690, 140);
contexte.lineTo(690, 180);
contexte.lineTo(136,180);

var gradient = contexte.createLinearGradient(130,0,170,0);
gradient.addColorStop(0, "rgba(216, 53, 10,0)");
gradient.addColorStop(1, "rgb(216, 53, 10)");
contexte.fillStyle = gradient;
contexte.strokeStyle = "transparent";

contexte.stroke();
contexte.fill();

// Partie rond
var chemin = [ [411.5,30.5], [719.5,60.5], [257.5,90.5], [565.5,90.5] , [103.5,150.5], [326.5,258.5] ];
contexte.fillStyle = "#0cff56";
for(var i = 0; i < chemin.length; i++){
    contexte.beginPath();
    contexte.arc(chemin[i][0], chemin[i][1], 4.5, 0, Math.PI * 2, true);
    contexte.fill();
}
contexte.moveTo(0,180);

// TRIANGLE
contexte.beginPath();
contexte.moveTo(560,33.5);
contexte.lineTo(570, 33.5);
contexte.lineTo(565, 23);
contexte.fillStyle = "#170cff";

contexte.fill();