import { disableScroll, enableScroll } from './Scroll';

export function burger() {
    let burger = document.getElementById('header__burger');
    let elementburg = document.querySelector('.header__nav--left');
    let activelink = elementburg.firstElementChild;
    let scroll = true;
    burger.addEventListener("click", () => {
        activelink.classList.remove("active");
        elementburg.classList.toggle('burger-active');
        burger.classList.toggle('active');
        if (scroll) {
            disableScroll();
            scroll = false;

        } else {
            enableScroll();
            scroll = true;
        }
    });
    window.addEventListener('resize', function(){
        var width = window.innerWidth;
        if (width > 580) {
            activelink.classList.add("active");
            document.querySelector('.header__nav--left').classList.remove('burger-active');
            document.getElementById('header__burger').classList.remove('active');

        }
    }, true);
}
