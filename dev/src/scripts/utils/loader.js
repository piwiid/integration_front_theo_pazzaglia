import {disableScroll, enableScroll} from "./Scroll";

export function loader() {
    var load = document.getElementById('loader');
    var roue = document.getElementsByClassName('roue');
    var roueDevant = document.getElementsByClassName('roue-devant');
    var roueArriere = document.getElementsByClassName('roue-arriere');
    var vitesse = document.getElementsByClassName('vitesse');


    disableScroll();
    TweenMax.to(roueDevant, 1.5, {
        transformOrigin: '50% 50%',
        rotation: '360',
        repeat: -1,
        ease: Power0.easeNone,
    });
    TweenMax.to(roueArriere, 1.5, {
        transformOrigin: '50% 50%',
        rotation: '360',
        delay: 0.2,
        repeat: -1,
        ease: Power0.easeNone,
    });
    TweenMax.to(vitesse, 4, {
        transformOrigin: '50% 50%',
        rotation: '360',
        repeat: -1,
        ease: Power0.easeNone,
    });
    setTimeout(function(){
        // Obliger d'utiliser height pour faire mon animation
        TweenMax.to(load, 2, {
            height: 0,
            ease:new Ease(BezierEasing(0.8,0,0.2,1)),
        });
    },2000);

    setTimeout(function(){
        enableScroll();
        load.remove();

    }, 4000);

}