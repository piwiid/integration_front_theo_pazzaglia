import fixIe from './utils/fixIE';
import Scroll from './libs/ScrollMagic.min';
import Cubic from './components/BezierEasing';
import Canvas from './components/canvas';
import {TweenMax} from 'gsap';
import CSSRulePlugin from "gsap/CSSRulePlugin";
import { burger } from './utils/Burger';
import { loader } from './utils/loader';
import { animatsionBestSeller, scrollBest } from './components/animation';
import Slider from './components/Slider/Slider.js';
import BestSeller from './components/Best-seller/Best-seller';


loader();
// Création du slider

let slider = new Slider(document.querySelector('.nouveaute__slider-nouveaute'));
slider.init('./json/slider.json');


// Création des best-seller
var listArticle = new BestSeller(document.getElementById('slider'));
listArticle.init('./json/best-seller.json');

var listArticle2 = new BestSeller(document.getElementById('all-model__slide'));
listArticle2.init('./json/best-model.json');
listArticle2.searchItem(document.getElementById('search'));
// Load burger
burger();

scrollBest('.information', 'li');
