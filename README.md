###Version débutant
# Installation du dépôt #

### Comment installer le dépôt ###

Dans un premier il faut cloner le projet.
Veuillez suivre les étapes suivantes :

Si vous êtes utilisateur Windows démarré Wamp.

Si vous êtes utilisateur Mac démarré Mamp. 

* Lancer le terminal
* Il faut choisir le dossier de reception
* Tapez "cd" + "mamp/wamp" + "chemin de reception" dans le dossier www de Wamp ou Mamp
* Pour cloner le projet tapez la commande dans le terminal "git clone lienduprojet"


### Installation du projet ###

Pour installer le projet il vous faut node.js sur votre ordinateur.

Si vous n'avez pas node.js voici un lien pour l'installer https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/installer-node-js

Après avoir cloné le projet, il faut indiquer au terminal que nous allons travailler dans notre dépôt, tapez la commande "cd fichiercloner/dev"

* Pour installer le projet veuillez taper la commande "npm install", cette commande permet de lire le fichier "package.json" qui va contenir nos modules.

Ça y est vous avez installer le dépot il manque plus qu'a le configurer.

### Configurer le dépôt ###

* Après avoir installé le projet il faut se diriger dans le fichier dev/gulp/config.js pour configurer le module browserSync (il permet de rafraîchir le navigateur quand on modifie un fichier scss, js ou php)'
* Sur la ligne 22 il faut modifier le proxy nommez le par exemple "gobelins.localhost"
* Il faut maintenant créer un vhost 

Si vous êtes sur Mac lancé Mamp cliquez sur hosts : 
* Ajouter un host gobelins.localhost
* Puis indiquez le chemin du projet "front/dev"

Vérifiez que vous avez bien configurer vos ports en cliquant sur Ports > Set ports to 80, 81, 443, 7443 & 3306

Si vous êtes sur Windows lancé Wamp ajoutez un host en cliquand sur Wamp>Vos VirtualsHosts gestion VirtualHost.

* Suivez les mêmes étapes que Mamp

### Démarrer le projet ###

Après avoir suivi les étapes.

Tapez dans le terminal cd front/dev.

Tapez npm start

Ça y est vous avez démarré le projet.

Sinon pour regarder le projet vous pouvez aller dans localhost/front/www.

#Version dev
Terminal

* git clone project
* cd front/dev
* npm install
* Config proxy with vhost on dev/gulp/config.js line 22

After that 
* npm start
